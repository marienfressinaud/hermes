<?php

namespace Minz;

trait Validable
{
    public function validate(): array
    {
        $classReflection = new \ReflectionClass(self::class);
        $properties = $classReflection->getProperties();

        $errors = [];
        foreach($properties as $property) {
            $check_attributes = $property->getAttributes(
                Validable\Check::class,
                \ReflectionAttribute::IS_INSTANCEOF
            );

            if (empty($check_attributes)) {
                continue;
            }

            $property_errors = [];
            foreach ($check_attributes as $reflection_attribute) {
                $attribute = $reflection_attribute->newInstance();
                $attribute->instance = $this;
                $attribute->property = $property;
                if (!$attribute->assert()) {
                    $property_errors[] = $attribute->getMessage();
                }
            }

            if ($property_errors) {
                $property_name = $property->getName();
                $errors[$property_name] = implode(' ', $property_errors);
            }
        }

        return $errors;
    }
}
