<?php

namespace App;

class Cli
{
    private $engine;

    public function __construct()
    {
        $router = new \Minz\Router();
        $router->addRoute('cli', '/system/init', 'System#init');
        $router->addRoute('cli', '/system/update', 'System#update');

        $router->addRoute('cli', '/feeds', 'Feeds#index');
        $router->addRoute('cli', '/feeds/add', 'Feeds#add');
        $router->addRoute('cli', '/feeds/sync', 'Feeds#sync');
        $router->addRoute('cli', '/feeds/unlock', 'Feeds#unlock');

        $router->addRoute('cli', '/entries', 'Entries#index');
        $router->addRoute('cli', '/entries/sync', 'Entries#sync');
        $router->addRoute('cli', '/entries/unlock', 'Entries#unlock');

        $router->addRoute('cli', '/news/generate', 'News#generate');

        $router->addRoute('cli', '/jobs', 'Jobs#index');
        $router->addRoute('cli', '/jobs/install', 'Jobs#install');
        $router->addRoute('cli', '/jobs/run', 'Jobs#run');
        $router->addRoute('cli', '/jobs/unlock', 'Jobs#unlock');
        $router->addRoute('cli', '/jobs/watch', 'Jobs#watch');

        $this->engine = new \Minz\Engine($router);
        \Minz\Url::setRouter($router);
    }

    /**
     * Execute a request.
     *
     * @param \Minz\Request $request
     *
     * @return \Minz\Response
     */
    public function run($request)
    {
        $bin = $request->param('bin');
        $bin = $bin === 'cli' ? 'php cli' : $bin;

        $current_command = $request->path();
        $current_command = trim(str_replace('/', ' ', $current_command));

        \Minz\Output\View::declareDefaultVariables([
            'bin' => $bin,
            'current_command' => $current_command,
        ]);

        return $this->engine->run($request, [
            'not_found_view_pointer' => 'cli/not_found.txt',
            'internal_server_error_view_pointer' => 'cli/internal_server_error.txt',
            'controller_namespace' => '\\App\\cli',
        ]);
    }
}
