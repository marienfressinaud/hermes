CREATE TABLE tokens (
    id TEXT NOT NULL PRIMARY KEY,
    created_at TEXT NOT NULL,
    expired_at TEXT NOT NULL
);

CREATE TABLE people (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    created_at TEXT NOT NULL,

    nickname TEXT UNIQUE NOT NULL,
    password_hash TEXT NOT NULL,

    session_token_id TEXT,
    FOREIGN KEY (session_token_id) REFERENCES tokens(id) ON UPDATE RESTRICT ON DELETE SET NULL
);

CREATE TABLE jobs (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    created_at TEXT NOT NULL,
    perform_at TEXT NOT NULL,
    name TEXT NOT NULL DEFAULT '',
    handler TEXT NOT NULL DEFAULT '{}',
    frequency TEXT NOT NULL DEFAULT '',
    queue TEXT NOT NULL DEFAULT 'default',
    locked_at TEXT,
    number_attempts BIGINT NOT NULL DEFAULT 0,
    last_error TEXT NOT NULL DEFAULT '',
    failed_at TEXT
);

CREATE TABLE feeds (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    created_at TEXT NOT NULL,
    url TEXT NOT NULL,
    type TEXT NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    links TEXT NOT NULL DEFAULT '{}',
    image_filename TEXT NOT NULL DEFAULT '',
    image_fetched_at TEXT,

    last_hash TEXT NOT NULL DEFAULT '',
    locked_at TEXT,
    fetched_at TEXT,
    fetched_code INTEGER NOT NULL DEFAULT 0,
    fetched_error TEXT NOT NULL DEFAULT ''
);

CREATE INDEX idx_feeds_fetched_at ON feeds(fetched_at);

CREATE TABLE entries (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    created_at TEXT NOT NULL,

    url TEXT NOT NULL,
    url_hash TEXT NOT NULL,
    title TEXT NOT NULL,
    published_at TEXT NOT NULL,
    reading_time INTEGER NOT NULL DEFAULT 0,
    links TEXT NOT NULL DEFAULT '{}',
    image_filename TEXT NOT NULL DEFAULT '',

    feed_id INTEGER NOT NULL,
    feed_entry_id TEXT NOT NULL DEFAULT '',

    in_news_of TEXT,

    locked_at TEXT,
    fetched_at TEXT,
    fetched_code INTEGER NOT NULL DEFAULT 0,
    fetched_error TEXT NOT NULL DEFAULT '',
    fetched_count INTEGER NOT NULL DEFAULT 0,

    FOREIGN KEY (feed_id) REFERENCES feeds(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX idx_entries_url ON entries(url_hash);
CREATE INDEX idx_entries_published_at ON entries(published_at);
CREATE INDEX idx_entries_fetched_at ON entries(fetched_at) WHERE fetched_at IS NULL;
CREATE INDEX idx_entries_fetched_code ON entries(fetched_code) WHERE fetched_code < 200 OR fetched_code >= 300;

CREATE TABLE votes (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    created_at TEXT NOT NULL,

    person_id INTEGER NOT NULL,
    entry_id INTEGER NOT NULL,
    strength TINYINT NOT NULL,

    FOREIGN KEY (person_id) REFERENCES people(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (entry_id) REFERENCES entries(id) ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE(person_id, entry_id) ON CONFLICT ABORT
);
