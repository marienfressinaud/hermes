<?php

namespace App\services;

use App\models;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class FeedFetcher
{
    /** @var \SpiderBits\Cache */
    private $cache;

    /** @var \SpiderBits\Http */
    private $http;

    /** @var array */
    private $options = [
        'timeout' => 20,
        'cache' => true,
    ];

    /**
     * @param mixed[] $options
     *     A list of options where possible keys are:
     *     - timeout (integer)
     *     - cache (boolean)
     */
    public function __construct(array $options = [])
    {
        $this->options = array_merge($this->options, $options);

        $cache_path = \Minz\Configuration::$application['cache_path'];
        $this->cache = new \SpiderBits\Cache($cache_path);

        $this->http = new \SpiderBits\Http();
        $this->http->user_agent = \Minz\Configuration::$application['user_agent'];
        $this->http->timeout = $this->options['timeout'];
    }

    public function fetch(models\Feed $feed): void
    {
        // First, fetch the info of the feed
        $info = $this->fetchUrl($feed->url);

        // Then, synchronize the feed
        $feed->fetched_at = \Minz\Time::now();
        $feed->fetched_code = $info['status'];
        $feed->fetched_error = '';
        if (isset($info['error'])) {
            $feed->fetched_error = $info['error'];
            $feed->save();
            return;
        }

        $spider_feed = $info['feed'];
        $hash = $spider_feed->hash();

        if ($hash === $feed->last_hash) {
            // The feed didn’t change, do nothing
            $feed->save();
            return;
        }

        $feed->last_hash = $hash;
        $feed->type = $spider_feed->type;

        $title = trim($spider_feed->title);
        if ($title && $feed->name === $feed->url) {
            $feed->name = $title;
        }

        $description = trim($spider_feed->description);
        if ($description) {
            $feed->description = $description;
        }

        if ($spider_feed->link) {
            $site_url = \SpiderBits\Url::absolutize($spider_feed->link, $feed->url);
            $site_url = \SpiderBits\Url::sanitize($site_url);
        } else {
            $site_url = $feed->url;
        }

        $feed->links['alternate'] = $site_url;

        $feed->save();

        // Finally, synchronize the entries
        $urls_mapping = models\Entry::getUrlsMapping($feed);
        $urls_indexed_by_entry_ids = models\Entry::getFeedEntryIdsMapping($feed);

        $entries_to_create = [];

        foreach ($spider_feed->entries as $spider_entry) {
            if (!$spider_entry->link) {
                continue;
            }

            $url = \SpiderBits\Url::absolutize($spider_entry->link, $feed->url);
            $url = \SpiderBits\Url::sanitize($url);

            if (isset($urls_mapping[$url])) {
                // The URL is already associated to the feed, we have
                // nothing more to do.
                continue;
            }

            if ($spider_entry->published_at) {
                $published_at = $spider_entry->published_at;
            } else {
                $published_at = \Minz\Time::now();
            }

            if ($spider_entry->id) {
                $feed_entry_id = $spider_entry->id;
            } else {
                $feed_entry_id = $url;
            }

            if (
                isset($feed_entry_ids_mapping[$feed_entry_id]) &&
                $feed_entry_ids_mapping[$feed_entry_id]['url'] !== $url
            ) {
                // We detected an entry with the same feed id and a different
                // URL. This can happen if the URL was changed by the publisher
                // after our first fetch. Normally, there is a redirection on
                // the server so it's not a big deal to not track this change,
                // but it duplicates content.
                // To avoid this problem, we update the URL and reset the
                // title and fetched_at so the entry is resynchronised by the
                // EntryFetcher service.
                $entry_id = $feed_entry_ids_mapping[$feed_entry_id]['id'];
                models\Entry::update($entry_id, [
                    'url' => $url,
                    'title' => $url,
                    'published_at' => $published_at,
                    'fetched_at' => null,
                ]);
            } else {
                $entry = models\Entry::init($url, $feed);
                $title = trim($spider_entry->title);
                if ($title) {
                    $entry->title = $title;
                }

                $entry->created_at = \Minz\Time::now();
                $entry->published_at = $published_at;
                $entry->feed_entry_id = $feed_entry_id;

                foreach ($spider_entry->links as $link_rel => $link_url) {
                    $link_url = \SpiderBits\Url::sanitize($link_url);
                    if (models\Entry::validateUrl($link_url)) {
                        $entry->links[$link_rel] = $link_url;
                    }
                }

                $entries_to_create[] = $entry;
            }
        }

        // We now can insert the entries
        models\Entry::bulkInsert($entries_to_create);
    }

    /**
     * Fetch URL content and return information about the feed
     *
     * @return mixed[] Possible keys are:
     *     - status (always)
     *     - error
     *     - feed
     */
    public function fetchUrl(string $url): array
    {
        // First, we "GET" the URL...
        $url_hash = \SpiderBits\Cache::hash($url);
        $cached_response = $this->cache->get($url_hash, 60 * 60);
        if ($this->options['cache'] && $cached_response) {
            // ... via the cache
            $response = \SpiderBits\Response::fromText($cached_response);
        } else {
            // ... or via HTTP
            try {
                $response = $this->http->get($url, [], [
                    'max_size' => 20 * 1024 * 1024,
                ]);
            } catch (\SpiderBits\HttpError $e) {
                return [
                    'status' => 0,
                    'error' => $e->getMessage(),
                ];
            }

            // that we add to cache on success
            if ($response->success) {
                $this->cache->save($url_hash, (string)$response);
            }
        }

        $info = [
            'status' => $response->status,
        ];

        if (!$response->success) {
            $encodings = mb_list_encodings();
            $data = mb_convert_encoding($response->data, 'UTF-8', $encodings);

            // Okay, Houston, we've had a problem here. Return early, there's
            // nothing more to do.
            $info['error'] = $data;
            return $info;
        }

        $content_type = $response->header('content-type');
        if (!\SpiderBits\feeds\Feed::isFeedContentType($content_type)) {
            $info['error'] = "Invalid content type: {$content_type}";
            return $info;
        }

        try {
            $feed = \SpiderBits\feeds\Feed::fromText($response->data);
            $info['feed'] = $feed;
        } catch (\Exception $e) {
            $info['error'] = (string)$e;
        }

        return $info;
    }
}
