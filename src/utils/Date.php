<?php

namespace App\utils;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Date
{
    public static function getBeginningDay(): \DateTime
    {
        $now = \Minz\Time::now();
        $seven_am = \Minz\Time::relative('today 7:00');

        if ($now < $seven_am) {
            // It is not 7 AM yet, so we list the entries after yesterday 7 AM.
            $seven_am->modify('-1 day');
        }

        return $seven_am;
    }
}
